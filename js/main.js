function generateAdditionalFields(){
    type=document.querySelector('#productType').value
    if(type=="Book"){
        document.querySelector(".additional").innerHTML='<input id="weight" name="weight" type="number" class="WeightField" placeholder="Write Weight here">'
    }
    else if(type=="DVD"){
        document.querySelector(".additional").innerHTML='<input id="size" name="size" type="number" class="SizeField" placeholder="Write Size here">'
    }
    if(type=="Furniture"){
        document.querySelector(".additional").innerHTML='<input id="length" name="length" type="number" class="LengthField" placeholder="Write Length here">'
        document.querySelector(".additional").innerHTML+='<input id="height" name="height" type="number" class="HeightField" placeholder="Write Height here">'
        document.querySelector(".additional").innerHTML+='<input id="width" name="width" type="number" class="WidthField" placeholder="Write Width here">'

    }
}

function Validate(){
    inputs=document.querySelectorAll('input')
    reg = new RegExp(/^[A-Za-z][A-Za-z0-9]*$/);
    for (let i = 0; i < inputs.length; i++) {
        if(inputs[i].value.length==0){
            alert("Please Fill in The" + inputs[i].className)
            return 0
        }
        else if (!(reg.test(inputs[i].value)) && inputs[i].getAttribute('type')=='text'){
            alert("Please follow inputs rules of text input Letters123")
            return 0
        }

      }      
      document.querySelector('form').submit()

}

skusToBeDeleted=[]
function arrayRemove(arr, value) { 
    
    return arr.filter(function(ele){ 
        return ele != value; 
    });
}
function Handler(e){
    var target = e.target
    if (target.className=='delete-checkbox')
    {
        if (target.checked && !skusToBeDeleted.includes(target.value)){
            skusToBeDeleted.push(target.value)
        }
        else if (!target.checked && skusToBeDeleted.includes(target.value)){
            skusToBeDeleted=arrayRemove(skusToBeDeleted,target.value)
        }
    }
}


if (window.location.href=='https://edutask.000webhostapp.com/addProduct'){
    generateAdditionalFields()
    document.querySelector('#productType').addEventListener("change", generateAdditionalFields)
    document.querySelector('.submit').addEventListener("click",Validate)
}





if (window.location.href=='https://edutask.000webhostapp.com/'){
    if (document.body.addEventListener){
    document.body.addEventListener('click',Handler,false)
    }
    else{
    document.body.attachEvent('onclick',Handler)
    }

document.querySelector('#addProduct').addEventListener("click",function(){
    window.location.href='/addProduct'
})
}




