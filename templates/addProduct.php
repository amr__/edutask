<html>
    <head>
        <title></title>
    </head>
    <body>
        <form id="product_form" action="/" method="post" onsubmit="event.preventDefault();">
            <input id="sku" name="SKU" type="text" class="SKUField" placeholder="Write SKU here">
            <input id="name" name="name"type="text" class="NameField" placeholder="Write Name here">
            <input id="price" name="price"type="number" class="PriceField" placeholder="Write Price here">
            <select id="productType" name="type" id="type">
                <option value="Book">Book</option>
                <option value="DVD">DVD</option>
                <option value="Furniture">Furniture</option>
            </select>
            <div class="additional"></div>
            <input type="submit" value="Save" class="submit">
            <a class="del submit" href="https://edutask.000webhostapp.com/">Cancel</a>
        </form>
    </body>
</html>