<head>
<title>Book</title>
</head>
<body>
<div class="buttons">
<input id="addProduct" class="submit" type="button" value="ADD">
<form id="delete" action="/" method="post">
<input type="submit" id="MassDel" class="submit del" type="button" value="MASS DELETE">
</div>
<div class="container">
<?php foreach ($products as $product):?>
<ul>
<input class="delete-checkbox" type="checkbox" name="skus[]" value="<?=$product->getSKU()?>">
<li><?=$product->getSKU()?></li>
<li><?=$product->getName()?></li>
<li><?=$product->getPrice()?> $</li>
<?php if(get_class($product)=='App\Products\Book'):?>
<li>Weight: <?=$product->getWeight()?>KG</li>
<?php elseif(get_class($product)=='App\Products\DVD'):?>
<li>Size: <?=$product->getSize()?></li>
<?php elseif(get_class($product)=='App\Products\Furniture'):?>
<li>Dimension: <?=$product->getLength()?>x<?=$product->getHeight()?>x<?=$product->getWidth()?></li>
<?php endif; ?>
</ul>
<?php endforeach;?>
</form>
</div>
</body>
</html>