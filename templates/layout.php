<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="styles/main.css">
<meta charset='utf-8'>
<title><?= $title; ?></title>
</head>
<body>
<div class='content'>
<?= $content; ?>
</div>
</body>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="js/main.js"></script>
</html>