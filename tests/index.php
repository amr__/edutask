<?php
include_once('./classes/Template.php');
include_once('./classes/Book.php');
include_once('./classes/DVD.php');
include_once('./classes/Furniture.php');
include_once('./classes/FrontController.php');
include_once('./classes/tracer.php');
$template=Template::getInstance();
//creating objects of book, dvd, furniture
$inputBook=array(
    'SKU'=>"bcmyiw234",
    'name'=>"this is book 1",
    'price'=>35,
    'weight'=>20,
);
$book= new Book($inputBook);
$book2= new Book($inputBook);
$book3= new Book($inputBook);
$book4= new Book($inputBook);

$inputDvd=array(
    'SKU'=>"ACJKE33450",
    'name'=>"this is dvd 1",
    'price'=>800,
    'size'=>90,
);
$dvd=new DVD($inputDvd);

$inputFurn=array(
    'SKU'=>"ALURVE334567",
    'name'=>"this is furn 1",
    'price'=>5000,
    'length'=>190,
    'width'=>200,
    'height'=>300,
);
$furniture=new Furn($inputFurn);

//defining paths to object templates
$bookPath="./templates/book.php";
$dvdPath="./templates/dvd.php";
$furnPath="./templates/furniture.php";
$productPath="./templates/products.php";
$layoutPath="./templates/layout.php";
$addFormPath="./templates/addProduct.php";
//echo $template->render($book_path,array('book'=>$book));
//echo $template->render($dvd_path,array('dvd'=>$dvd));
//echo $template->render($furn_path,array('furn'=>$furniture));
$products=$template->render($productPath,array('products'=>array($book,$book2,$book3,$book4,$dvd,$furniture)));
echo $template->render($layoutPath,array('title'=>"Listing products",'content'=>$products));

/* $instance1= FrontController::getInstance();
$instance2= FrontController::getInstance();
if ($instance1===$instance2){
    echo "Identical instances";
    echo Tracer::trace($instance1);
}
else {
    echo "Not Identical";
} */

/* echo Tracer::trace($_SERVER);
echo Tracer::trace($_GET);
echo Tracer::trace($_POST); */

/* foreach(array($book,$dvd,$furniture) as $product){
    echo $product;
} */

?>