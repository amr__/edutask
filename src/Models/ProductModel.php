<?php
namespace App\Models;
use FFI\Exception;
use mysqli;
use App\Products\Book;
use App\Products\DVD;
use App\Products\Furniture;

class ProductModel{

    private $dbConnection;

    private function __construct()
    {
        try{
        $this->dbConnection =new mysqli('localhost','id18776958_root','Scandi@353535','id18776958_my_db');
        }
        catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public static function getInstance(){
        static $instance;
        if (!isset($instance)){
            $instance=new ProductModel();
            return $instance;
        }
        else{
            return $instance;
        }
    }

    public function getDBConnection(){
    return $this->dbConnection;
  }
    
    public function addProduct($product){
        $classTypeTree=explode('\\',strtolower(get_class($product)));
        $productArr=array(
            'SKU'=>$product->getSKU(),
            'name'=>$product->getName(),
            'price'=>$product->getPrice(),
            //ENUM in database has furn not furniture
            'type'=>end($classTypeTree)=='furniture'? 'furn': end($classTypeTree),
            'size'=> method_exists($product,'getSize') ? $product->getSize() : "NULL",
            'weight'=> method_exists($product,'getWeight') ? $product->getWeight() : "NULL",
            'length'=> method_exists($product,'getLength') ? $product->getLength() : "NULL",
            'height'=> method_exists($product,'getHeight') ? $product->getHeight() : "NULL",
            'width'=> method_exists($product,'getWidth') ? $product->getWidth() : "NULL",
        );

        $query="INSERT INTO Products (SKU,name,price,type,size,weight,lenght,height,width) VALUES(
        '{$productArr['SKU']}','{$productArr['name']}',{$productArr['price']},'{$productArr['type']}',{$productArr['size']},{$productArr['weight']},{$productArr['length']},{$productArr['height']},{$productArr['width']})";

        try{
            $this->dbConnection->query($query);
            }
            catch(Exception $e){
                echo $e->getMessage();
            }

    }
    
    public function getProducts(){

        $query="SELECT * FROM Products ORDER BY SKU ASC";
        $result_arr=array();

        try {
            $result = $this->dbConnection-> query($query); 
            while ($row = $result -> fetch_row()) {
                if ($row[3]=="book"){
                    $book_arr=array(
                        'SKU'=>$row[0],
                        'name'=>$row[1],
                        'price'=>$row[2],
                        'weight'=>$row[5],
                    );
                    $book= new Book($book_arr);
                    $result_arr[]=$book;
                }
                else if ($row[3]=="dvd"){
                    $dvd_arr=array(
                        'SKU'=>$row[0],
                        'name'=>$row[1],
                        'price'=>$row[2],
                        'size'=>$row[4],
                    );
                    $dvd= new DVD($dvd_arr);
                    $result_arr[]=$dvd;
                }
                else if ($row[3]=="furn"){
                    $furn_arr=array(
                        'SKU'=>$row[0],
                        'name'=>$row[1],
                        'price'=>$row[2],
                        'length'=>$row[6],
                        'width'=>$row[7],
                        'height'=>$row[8]
                    );
                    $furn= new Furniture($furn_arr);
                    $result_arr[]=$furn;
                }
                }
              
    }
    catch(Exception $e){
        echo $e->getMessage();
    }
    return $result_arr;
}

    public function delProducts($skus){
        $skus_arr="('".implode("','",$skus)."')";
        $query="DELETE FROM Products WHERE SKU IN $skus_arr";
        try {
            $this->dbConnection->query($query);
         }
         catch (Exception $e) {
            echo $e->getMessage();
         }
    }


}

?>