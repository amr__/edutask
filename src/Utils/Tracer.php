<?php
namespace App\Utils;

class Tracer{
    public function __construct()
    {       
        //if construction of Tracer is needed
    }

    public static function trace($unknown_var){
        echo '<pre>';
        var_dump($unknown_var);
        echo '</pre>';
    }

    public static function traceAndExit($unknown_var){
        self::trace($unknown_var);
        exit();
    }
}
?>