<?php
namespace App\Products;
use FFI\Exception;
use App\Products\Product;

class Furniture extends Product {
    private $length;
    private $width;
    private $height;

    //constrctor

    public function __construct($data)
    {
        parent::__construct($data);
        $this->setLength($data['length']);
        $this->setWidth($data['width']);
        $this->setHeight($data['height']);
    }

    public function renderHTML(){
        try{
        ob_start();
        include('./templates/furniture.php');
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
        return ob_get_clean();
    }
    public function __toString()
    {
        return $this->renderHTML();
    }
//getters

    public function getLength(){
        return $this->length;
    }

    public function getWidth(){
        return $this->width;
    }

    public function getHeight(){
        return $this->height;
    }
//setters

    public function setLength($length){
        $this->length=$length;
    }

    public function setWidth($width){
        $this->width=$width;
    }

    public function setHeight($height){
        $this->height=$height;
    }
    
}


?>