<?php
namespace App\Products;
use FFI\Exception;
use App\Products\Product;

class DVD extends Product {
    private $size;
    //constrctor

    public function __construct($data)
    {
        parent::__construct($data);
        $this->setSize($data['size']);
    }

    public function renderHTML(){
        try{
        ob_start();
        include('./templates/dvd.php');
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
        return ob_get_clean();
    }
    public function __toString()
    {
        return $this->renderHTML();
    }
//getters

    public function getSize(){
        return $this->size;
    }
//setters

    public function setSize($size){
        $this->size=$size;
    }
    
}


?>