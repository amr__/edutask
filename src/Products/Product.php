<?php
namespace App\Products;

abstract class  Product {
    private $SKU;
    private $name;
    private $price;


    //constructor

    public function __construct($data)
    {
        $this->setSKU($data['SKU']);
        $this->setName($data['name']);
        $this->setPrice($data['price']);
    }
//getters
    public function getSKU(){
        return $this->SKU;
    }

    public function getName(){
        return $this->name;
    }

    public function getPrice(){
        return $this->price;
    }

//setters
    public function setSKU($SKU){
        $this->SKU=$SKU;
    }

    public function setName($name){
        $this->name=$name;
    }

    public function setPrice($price){
        $this->price=$price;
    }
    
}


?>