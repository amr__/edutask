<?php
namespace App\Products;
use FFI\Exception;
class Book extends Product {
    private $weight;

    //constrctor

    public function __construct($data)
    {
        parent::__construct($data);
        $this->setWieght($data['weight']);
    }

    public function renderHTML(){
        try{
        ob_start();
        include('./templates/book.php');
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
        return ob_get_clean();
    }
    public function __toString()
    {
        return $this->renderHTML();
    }

//getters

    public function getWeight(){
        return $this->weight;
    }
//setters

    public function setWieght($weight){
        $this->weight=$weight;
    }
    
}


?>