<?php
namespace App\Views;

class Template{
    private function __construct()
    {
        
    }
    public static function getInstance(){
        static $instance;
        if (!isset($instance)){
            $instance=new Template();
            return $instance;
        }
        else{
            return $instance;
        }
    }
    public function render($template,$data=array()){
        try{
        ob_start();
        extract($data);
        include($template);
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
        return ob_get_clean();
    }
}
?>