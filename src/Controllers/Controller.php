<?php
namespace App\Controllers;
use App\Models\ProductModel;
use App\Views\Template;
abstract class Controller{
    private $model;
    private $view;
    public function __construct()
    {
        $this->model=ProductModel::getInstance();
        $this->view=Template::getInstance();
    }

    protected function getModel(){
        return $this->model;
    }

    protected function getView(){
        return $this->view;
    }
    abstract function Act();
}
?>