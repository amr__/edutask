<?php
namespace App\Controllers\ActionControllers;
use App\Controllers\Controller;
use App\Products\Book;
use App\Products\DVD;
use App\Products\Furniture;

class AddProductController extends Controller{
    public static function getInstance()
    {
        static $instance;
        if (!isset($instance)){
            $instance=new AddProductController();
            return $instance;
        }
        else{
            return $instance;
        }
    }
    public function Act(){
        $model=$this->getModel();
        $view=$this->getView();
        if($_POST['type']=='Book'){
            $book=new Book(array(
                'SKU'=>$_POST['SKU'],
                'name'=>$_POST['name'],
                'price'=>$_POST['price'],
                'weight'=>$_POST['weight']
            ));
            $model->addProduct($book);
        }
        else if($_POST['type']=='DVD'){
            $dvd=new DVD(array(
                'SKU'=>$_POST['SKU'],
                'name'=>$_POST['name'],
                'price'=>$_POST['price'],
                'size'=>$_POST['size']
            ));
            $model->addProduct($dvd);
        }
        else if($_POST['type']=='Furniture'){
            $furniture=new Furniture(array(
                'SKU'=>$_POST['SKU'],
                'name'=>$_POST['name'],
                'price'=>$_POST['price'],
                'length'=>$_POST['length'],
                'width'=>$_POST['width'],
                'height'=>$_POST['height']
            ));
            $model->addProduct($furniture);
        }
        $products=$model->getProducts();
        $productsView=$view->render('templates/products.php',array('products'=>$products));
        echo $view->render('templates/layout.php',array('title'=>"Listing products",'content'=>$productsView));
        }
}
?>