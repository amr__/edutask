<?php
namespace App\Controllers\ActionControllers;
use App\Controllers\Controller;
class DeleteProductController extends Controller{
    public static function getInstance()
    {
        static $instance;
        if (!isset($instance)){
            $instance=new DeleteProductController();
            return $instance;
        }
        else{
            return $instance;
        }
    }
    public function Act(){
        $model=$this->getModel();
        $view=$this->getView();
        $model->delProducts($_POST['skus']);
        $products=$model->getProducts();
        $productsView=$view->render('templates/products.php',array('products'=>$products));
        echo $view->render('templates/layout.php',array('title'=>"Listing products",'content'=>$productsView));

    }
}
?>