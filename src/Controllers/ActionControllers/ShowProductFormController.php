<?php
namespace App\Controllers\ActionControllers;
use App\Controllers\Controller;
class ShowProductFormController extends Controller{
    public static function getInstance()
    {
        static $instance;
        if (!isset($instance)){
            $instance=new ShowProductFormController();
            return $instance;
        }
        else{
            return $instance;
        }
    }
    public function Act(){
        $view=$this->getView();
        $addProductsView=$view->render('templates/addProduct.php');
        echo $view->render('templates/layout.php',array('title'=>"Adding products",'content'=>$addProductsView));
    }
}
?>