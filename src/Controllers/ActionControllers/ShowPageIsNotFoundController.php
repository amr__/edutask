<?php
namespace App\Controllers\ActionControllers;
use App\Controllers\Controller;
class ShowPageIsNotFoundController extends Controller{
    public static function getInstance()
    {
        static $instance;
        if (!isset($instance)){
            $instance=new ShowPageIsNotFoundController();
            return $instance;
        }
        else{
            return $instance;
        }
    }
    public function Act(){
        $view=$this->getView();
        echo $view->render('templates/layout.php',array('title'=>"URL Not Found",'content'=>"<h1> 404 Not Found</h1>"));
    }
}
?>