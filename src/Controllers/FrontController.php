<?php
namespace App\Controllers;
use App\Controllers\ActionControllers\AddProductController;
use App\Controllers\ActionControllers\DeleteProductController;
use App\Controllers\ActionControllers\ShowPageController;
use App\Controllers\ActionControllers\ShowPageIsNotFoundController;
use App\Controllers\ActionControllers\ShowProductController;
use App\Controllers\ActionControllers\ShowProductFormController;
class FrontController{
    private $url;
    private $httpMethod;
    private function __construct()
    {
        $this->url=$_SERVER['REDIRECT_URL'];
        $this->httpMethod=$_SERVER['REQUEST_METHOD'];
    }

    private function getUrl(){
        return $this->url;
    }

    private function getHttpMethod(){
        return $this->httpMethod;
    }

    public static function getInstance(){
        static $instance;
        if (!isset($instance)){
            $instance=new FrontController();
            return $instance;
        }
        else{
            return $instance;
        }
    }

    public function Route(){

        if($this->url=='/' && $this->httpMethod=='GET'){
            $controller=ShowProductController::getInstance();
            $controller->Act();
        }
        else if($this->url=='/addProduct' && $this->httpMethod=='GET'){
            $controller=ShowProductFormController::getInstance();
            $controller->Act();
        }
        else if($this->url=='/' && $this->httpMethod=='POST' && isset($_POST['name'])){
            $controller=AddProductController::getInstance();
            $controller->Act();
        }
        else if($this->url=='/' && $this->httpMethod=='POST' && isset($_POST['skus'])){
            $controller=DeleteProductController::getInstance();
            $controller->Act();
        }
        else if($this->url=='/' && $this->httpMethod=='POST' && !isset($_POST['skus'])){
            $controller=ShowPageController::getInstance();
            $controller->Act();
        }
        else{
            $controller=ShowPageIsNotFoundController::getInstance();
            $controller->Act();
        }
    }
}
?>